var Twitter = require('twitter');
var config = require('config');
var sleep = require('system-sleep');


var client = new Twitter({
  consumer_key: config.get('Twitter.consumer_key'),
  consumer_secret: config.get('Twitter.consumer_secret'),
  access_token_key: config.get('Twitter.access_token_key'),
  access_token_secret: config.get('Twitter.access_token_secret')
});

var ListOfIds = [];
var targetUser = 'datacircle_io';
var page_index = -1;

function suspend(value)
{
    console.log("will sleep");
    sleep(value * 1000);
    console.log("awoke!");
}

function getFollowing()
{
    console.log("Looking up who you follow. Page: " + page_index + '\n');
    client.get('friends/list', {count: 200, cursor: page_index}, function(error, tweets, response) {   
        if (response.body === undefined) {
            console.log(response.body);
            return;
        }

        if (error) {
            console.log(error);

            if (error[0].code == 88) {
                suspend(900);
                getFollowing();
            }

            return;
        }
        
        var data = JSON.parse(response.body);
        console.log(data.users.length);
        
        if (data.users.length == 0) {
            console.log(data)
            return;
        }

        for (var i = 0; i<data.users.length; i++)
        {
            console.log(".");                            
            //if (data.users[i].following == false) {
                UnFollowUser(data.users[i].id);
            //}
        }

        if (data.next_cursor == page_index) {
            return;
        } else {
            page_index = data.next_cursor;
            getFollowing()
        }
    });
}

getFollowing();


function UnFollowUser(UserId)
{
    console.log("Following: " + UserId + '\n');    
    client.post('friendships/destroy', {user_id: UserId},  function(error, tweet, response) {
        if (error) {
            console.log("Failed! " + error + '\n');  
        }
        else
        {
            console.log("success!");            
        }
    });
}