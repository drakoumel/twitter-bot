var Twitter = require('twitter');
var config = require('config');
var sleep = require('system-sleep');


var client = new Twitter({
  consumer_key: config.get('Twitter.consumer_key'),
  consumer_secret: config.get('Twitter.consumer_secret'),
  access_token_key: config.get('Twitter.access_token_key'),
  access_token_secret: config.get('Twitter.access_token_secret')
});

var ListOfIds = [];
var targetUser = 'getpenta';
var page_index = -1;

function suspend(value)
{
    console.log("will sleep");
    sleep(value * 1000);
    console.log("awoke!");
}

function search()
{
    client.get('search/tweets', {q: '#', count: 100}, function(error, tweets, response) {   
        if (response.body === undefined) {
            console.log(response.body);
            return;
        }

        if (error) {
            console.log(error);

            if (error[0].code == 88) {
                console.log("-------------------------LIMITED END");
            }

            return;
        }

        var data = JSON.parse(response.body);
        console.log(data.statuses.length + '\n');

        for (var i = 0; i<data.statuses.length -1; i++)
        {
            if (data.statuses[i].user.following == false) {
                FollowUser(data.statuses[i].user.screen_name)
                suspend(6);
            }
        }        
    });
}

search();

function FollowUser(screenName)
{
    console.log("Following: " + screenName + '\n');    
    client.post('friendships/create', {screen_name: screenName},  function(error, tweet, response) {
        if (error) {
            console.log("Failed! " + error + '\n');  
            console.log(error);
        }
        else
        {
            console.log("success!");            
        }
    });
}